/***
 *  $Id$
 **
 *  File: mpsb_mpi.cpp
 *  Created: Nov 17, 2022
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2022 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#include <chrono>
#include <iostream>
#include <vector>
#include <string>

#include <csv.hpp>

#include <mpi_executor.hpp>

#include "mpsb_state.hpp"
#include "mpsb_task.hpp"


template<int N, typename container, typename T, typename Args>
void mpsb_search(MPI_Comm Comm, const container& D, const int& argc, const Args& argv, const T& t) {
    using task_type = mpsb_task<N>;
    using state_type = mpsb_state<N>;
    using partitioner_type = hash_partitioner<N>;

    int rank, size;
    MPI_Comm_rank(Comm, &rank);
    MPI_Comm_size(Comm, &size);

    auto [res, n, m] = t;

    if (!res) {
        if (rank == 0) std::cout << "error: could not read csv file" << std::endl;
        return;
    }

    // initialize types
    task_type::n_ = n;
    task_type::m_ = m;
    task_type::score_ = sabnatk::create_RadCounter<N>(n, m, D.begin());

    state_type::n_ = n;

    // get priors
    if (argc == 5) {
        auto [res, _] = task_type::priors_.read(argv[4], task_type::n_);

        if (!res) {
            if (rank == 0) std::cout << "error: could not read priors file" << std::endl;
            return;
        }
    }

    // do final initialization
    if (argc > 3) task_type::pa_limit_ = std::atoi(argv[3]);

    task_type::set_bounds();

    scool::mpi_executor<task_type, state_type, partitioner_type, true> exec(MPI_COMM_WORLD);

    exec.log().level(mpix::Logger::DEBUG);

    exec.init(task_type(), state_type(), partitioner_type());
    auto t0 = std::chrono::steady_clock::now();

    exec.log().debug() << "processing with N = " << N << " words of size " << sizeof(bit_util_base_type) << "B" << std::endl;
    exec.log().info() << "MPS has " << n << " variables" << std::endl;
    exec.log().info() << "limit: " << task_type::pa_limit_ << std::endl;

     for (int i = 0; i < n; ++i) {
        auto t0 = std::chrono::steady_clock::now();
        if (exec.step() == 0) break;
        auto t1 = std::chrono::steady_clock::now();

        std::chrono::duration<double> diff = t1 - t0;
        exec.log().debug() << "time to step " << diff.count() << "s" << std::endl;
    }

    auto t1 = std::chrono::steady_clock::now();

    std::chrono::duration<double> diff = t1 - t0;
    exec.log().info() << "time to solution: " << diff.count() << "s" << std::endl;

    if (rank == 0) exec.state().mps_list.write(argv[2], task_type::norder_);

    return;
} // mpsb_search

int main(int argc, char* argv[]) {
    int tlevel, size, rank;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &tlevel);

    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (tlevel != MPI_THREAD_MULTIPLE) {
        if (rank == 0) std::cout << "error: insufficient threading support in MPI" << std::endl;
        return MPI_Finalize();
    }

    if (argc < 3) {
        if (rank == 0) std::cout << "usage: mpsb_mpi csvfile mpsfile [palimit priorsfile]" << std::endl;
        return MPI_Finalize();
    }

    using data_type = uint8_t;
    std::vector<data_type> D;

    auto t = read_csv(argv[1], D);

    const int n = get<1> (t);
    if (n <= set_max_item<1>()) mpsb_search<1>(MPI_COMM_WORLD, D, argc, argv, t);
    else if (n <= set_max_item<2>()) mpsb_search<2>(MPI_COMM_WORLD, D, argc, argv, t);
    else if (n <= set_max_item<3>()) mpsb_search<3>(MPI_COMM_WORLD, D,  argc, argv, t);
    else if (n <= set_max_item<4>()) mpsb_search<4>(MPI_COMM_WORLD, D, argc, argv, t);
    else {
        if (rank == 0) std::cout << "unable to handle more than " << set_max_item<4>() << " variables!";
    }

    return MPI_Finalize();
} // main
