/***
 *  $Id$
 **
 *  File: mpsb_mpi.cpp
 *  Created: Nov 17, 2022
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2022 - 2023 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#include <chrono>
#include <iostream>
#include <vector>

#include <csv.hpp>

#include <hpx_shm_executor.hpp>

#include "mpsb_state.hpp"
#include "mpsb_task.hpp"


const int N = 4;

using task_type = mpsb_task<N>;
using state_type = mpsb_state<N>;
using partitioner_type = hash_partitioner<N>;


int main(int argc, char* argv[]) {
    if (argc < 4) {
        std::cout << "usage: mpsb_mpi csvfile mpsfile [palimit priorsfile]" << std::endl;
        return 0;
    }

    int bucket_size = std::atoi(argv[3]);

    // get input data
    using data_type = uint8_t;

    int n = 0;
    int m = 0;
    std::vector<data_type> D;

    std::cout << argv[1] << "\n";

    bool res = false;
    std::tie(res, n, m) = read_csv(argv[1], D);

    if (!res) {
        std::cout << "error: could not read csv file" << std::endl;
        return 0;
    }

    // initialize types
    task_type::n_ = n;
    task_type::m_ = m;
    task_type::score_ = sabnatk::create_RadCounter<N>(n, m, D.begin());

    state_type::n_ = n;

    // get priors
    if (argc == 6) {
        std::tie(res, std::ignore) = task_type::priors_.read(argv[5], task_type::n_);

        if (!res) {
            std::cout << "error: could not read priors file" << std::endl;
        }
    }

    // do final initialization
    if (argc > 4) task_type::pa_limit_ = std::atoi(argv[4]);

    task_type::set_bounds();

    scool::hpx_shm_executor<task_type, state_type,partitioner_type, true> exec(bucket_size);

    // exec.log().level(mpix::Logger::DEBUG);
    exec.log().level(jaz::Logger::DEBUG);

    exec.init(task_type(), state_type(), partitioner_type());
    auto t0 = std::chrono::steady_clock::now();

     for (int i = 0; i < n; ++i) {
        auto t0 = std::chrono::steady_clock::now();
        if (exec.step() == 0) break;
        auto t1 = std::chrono::steady_clock::now();

        std::chrono::duration<double> diff = t1 - t0;
        exec.log().debug() << "time to step " << diff.count() << "s" << std::endl;
    }

    auto t1 = std::chrono::steady_clock::now();

    std::chrono::duration<double> diff = t1 - t0;
    exec.log().info() << "time to solution: " << diff.count() << "s" << std::endl;

    exec.state().mps_list.write(argv[2], task_type::norder_);

    return 0;
} // main
