/***
 *  $Id$
 **
 *  File: mpsb_state.hpp
 *  Created: Dec 05, 2022
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2022 - 2023 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#ifndef MPSB_STATE_HPP
#define MPSB_STATE_HPP

#include "MPSList.hpp"


template <int N> struct mpsb_state {

    mpsb_state() { mps_list.init(n_); }

    void identity() { }

    void operator+=(const mpsb_state& st) {
        mps_list.merge(st.mps_list);
    } // operator+=

    bool operator==(const mpsb_state& st) const {
        return (mps_list == st.mps_list);
    } // operator==


    MPSList<N> mps_list;
    static inline int n_ = 0;

}; // struct mpsb_state


template <int N>
inline std::ostream& operator<<(std::ostream& os, const mpsb_state<N>& st) {
    return os << st.mps_list;
} // operator<<

template <int N>
inline std::istream& operator>>(std::istream& is, mpsb_state<N>& st) {
    return is >> st.mps_list;
} // operator>>

#endif // MPSB_STATE_HPP
