/***
 *  $Id$
 **
 *  File: qap_hpx_shm.cpp
 *  Created: April 18, 2022
 *
 *  Author: Sai Vishwanath Venkatesh <saivishw@buffalo.edu>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2020-2022 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

#include <hpx_shm_executor.hpp>
#include <partitioner.hpp>

#include "qap_common.hpp"
#include "qap_state.hpp"
#include "qap_task.hpp"


int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "usage: qap_seq qaplib_instance bucket_size" << std::endl;
        return 0;
    }

    int bucket_size = std::atoi(argv[2]);

    if (read_qaplib_instance(argv[1], qap_task::n_, qap_task::F_, qap_task::D_)) {
        std::vector<int> res(qap_task::n_);
        std::iota(std::begin(res), std::end(res), 0);

        qap_task t(std::begin(res), std::end(res));
        qap_state st(qap_task::compute_cost(t.p_), t.p_);

        scool::hpx_shm_executor<qap_task, qap_state, qap_partitioner, true> exec(bucket_size);

        exec.init(t, st);

        auto t0 = std::chrono::steady_clock::now();

        while (exec.step() > 0) { }

        auto t1 = std::chrono::steady_clock::now();

        exec.log().info() << "final result:" << std::endl;
        exec.state().print(exec.log().info());

        std::chrono::duration<double> T = t1 - t0;
        exec.log().info() << "time to solution: " << T.count() << "s" << std::endl;
    } else {
        std::cout << "error: could not read instance" << std::endl;
        return -1;
    }

    return 0;
} // main
