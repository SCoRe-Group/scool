/***
 *  $Id$
 **
 *  File: bnsl_hpx_shm.cpp
 *  Created: Apr 1, 2022
 *
 *  Author: Zainul Abideen Sayed <zsayed@buffalo.edu>
 *          Sai Vishwanath Venkatesh <saivishw@buffalo.edu>
 *          Nithin Sastry Tellapuri <nithin_sastry@outlook.com>
 *  Copyright (c) 2020-2022 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

#include <hpx_shm_executor.hpp>
#include <partitioner.hpp>

#include "bnsl_task.hpp"
#include "bnsl_state.hpp"


const int N = 3;

using task_type = bnsl_task<N>;
using partitioner_type = bnsl_hyper_partitioner<N>;


void bnsl_search(int bucket_size) {
    task_type t;
    bnsl_state<task_type::set_type> st;

    // bnsl tasks are never unique, they form poset lattice
    scool::hpx_shm_executor<task_type, bnsl_state<task_type::set_type>, partitioner_type, false> exec(bucket_size);

    exec.init(t, st, partitioner_type());
    auto t0 = std::chrono::steady_clock::now();

    for (int i = 0; i <= t.n; ++i) {
        exec.step();
        exec.log().info() << "active tasks: " << exec.state().active_task << std::endl;
    }

    auto t1 = std::chrono::steady_clock::now();

    exec.log().info() << "final result:" << std::endl;
    exec.state().print(exec.log().info());

    std::chrono::duration<double> T = t1 - t0;
    exec.log().info() << "time to solution: " << T.count() << "s" << std::endl;
} // bnsl_search


int main(int argc, char* argv[]) {
    if (argc != 4) {
        std::cout << "usage: bnsl_mpi n mpsfile bucket_size" << std::endl;
        return 0;
    }

    int n = std::atoi(argv[1]);
    int bucket_size = std::atoi(argv[3]);

    task_type::n = n;
    task_type t;
    t.mps_list.init(n);
    auto res = task_type::mps_list.read(n, argv[2]);

    if (res.first) {
        // initialize remaining part of task_type
        task_type::target_id = set_full<task_type::set_type>(n);
        task_type::opt_pa.resize(n);

        for (int xi = 0; xi < n; ++xi) {
            auto opt = task_type::mps_list.optimal(xi);
            task_type::opt_pa[xi] = {opt.pa, opt.s};
        }

        // let's go searching
        bnsl_search(bucket_size);
    } else {
        std::cout << "error: " << res.second << std::endl;
    }

    return 0;
} // main
