/***
 *  $Id$
 **
 *  File: bnsl_task.hpp
 *  Created: Jan 13, 2022
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@hush.com>
 *          Zainul Abideen Sayed <zsayed@buffalo.edu>
 *  Copyright (c) 2022 - 2023 SCoRe Group
 *  Distributed under the MIT License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of SCoOL.
 */

#ifndef BNSL_TASK_HPP
#define BNSL_TASK_HPP

#include <iostream>
#include <unordered_map>
#include <vector>

#include <jaz/hash.hpp>

#include <MPSList.hpp>
#include <graph_util.hpp>


template <int N = 2> struct bnsl_task {
    static const int PATH_SIZE = set_max_item<N>() + 1;

    using set_type = uint_type<N>;

    set_type id = set_empty<set_type>();
    double score = 0.0;

    uint8_t path[PATH_SIZE];

    set_type suboptimal_extension = set_empty<set_type>();
    bool active = true;

    template <typename ContextType, typename StateType>
    void process(ContextType& ctx, StateType& st) const {
        if (score < 0.0) return;

        int l = ctx.iteration();
        int sz = set_size(id);

        // last task holds the solution
        if (sz == n) {
            if (score < st.score) {
                st.tid = id;
                st.score = score;
                st.path.resize(n);
                std::copy(path, path + n, std::begin(st.path));
            }
        }

        // if task is not ready for processing, we simply defer
        if (l < sz) {
            ctx.push(*this);
            st.empty_task++;
            return;
        }

        st.active_task++;
        bnsl_task t;

        // consider all possible extensions of the current node
        for (int xi = 0; xi < n; ++xi) {
            if (in_set(id, xi) || in_set(suboptimal_extension, xi)) continue;

            t.id = set_add(id, xi);
            t.score = score + mps_list.find(xi, id).s;

            std::copy(path, path + l, t.path);
            t.path[l] = xi;

            // apply OPE
            ope(target_id, l + 1, t);

            if (!ctx.push(t)) continue;

            // apply SCC
            auto temp = t;
            if(!scc(t)) continue;

            if ((temp.id != t.id)) {
                temp.score = -1.0;
                ctx.push(temp);
            }

            ctx.push(t);
        } // for xi
    } // process

    void merge(const bnsl_task& t) {
        if (t.score < score) {
            score = t.score;
            std::copy(t.path, t.path + PATH_SIZE, path);
            suboptimal_extension = t.suboptimal_extension;
        }
    } // merge

    static void vanilla_bfs(const set_type& to_add, set_type& source_id, bnsl_task& source_node) {
        int n = mps_list.n();

        int source_id_size = set_size(source_id);
        int to_add_size = set_size(to_add);
        int target_id_size = source_id_size + to_add_size;

        set_type goal_id = source_id;
        for (int x = 0; x < set_max_size<set_type>(); ++x)
            if(in_set(to_add, x)) goal_id = set_add(goal_id, x);

        // set_type goal_id = source_id;
        // for (int x : as_vector(to_add)) goal_id = set_add(goal_id, x);

        std::vector<std::unordered_map<set_type, bnsl_task, uint_hash>> vec_ht(target_id_size + 1);
        vec_ht[0].insert({source_id, source_node});

        for (int l = 0; l <= target_id_size; ++l) {
            if (vec_ht[l].size() == 1 && vec_ht[l].begin()->first == goal_id) {
                source_id = goal_id;
                source_node = vec_ht[l].begin()->second;
                return;
            }

            for (auto& pa_node : vec_ht[l]) {
                for (int xi = 0; xi < n; ++xi) {
                    if (in_set(to_add, xi) && !in_set(pa_node.first, xi)) {
                        bnsl_task ch_node = pa_node.second;
                        set_type ch_node_id = set_add(pa_node.first, xi);
                        ch_node.score += mps_list.find(xi, pa_node.first).s;

                        int ch_node_id_size = set_size(ch_node_id);
                        ch_node.path[ch_node_id_size - 1] = xi;

                        ope(to_add, ch_node_id_size, ch_node);

                        int idx = ch_node_id_size - source_id_size;
                        auto it = vec_ht[idx].find(ch_node_id);

                        if (it != vec_ht[idx].end()) {
                            if (ch_node.score < it->second.score) { it->second = ch_node; }
                        } else { vec_ht[idx].insert({ch_node_id, ch_node}); }
                    }
                } // for xi
            } // for pa_node
        } // for l
    } // vanilla_bfs

    static void ope(const set_type to_add, int l, bnsl_task& node) {
        bool extend = true;

        for (int i = 0; ((i < n) && extend); ++i) {
            extend = false;

            for (int xi = 0; xi < n; ++xi) {
                if (!in_set(node.id, xi) && is_superset(node.id, opt_pa[xi].first) && in_set(to_add, xi)) {
                    extend = true;
                    node.id = set_add(node.id, xi);
                    node.score += opt_pa[xi].second;
                    node.path[l] = xi;
                    l++;
                }
            } // for xi
        } // for i
    } // ope

    static bool scc(bnsl_task& nd) {
        set_type nd_id = nd.id;
        set_type to_add = set_diff(target_id, nd_id);
        std::vector<set_type> adj = mps_list.subset_adjacency_matrix(nd_id, to_add);
        nd.id = nd_id;

        // get DAG density
        int ne = 0;
        auto n = adj.size();

        for (auto& x : adj) ne += set_size(x);

        double dd = (1.0 * ne) / (n * n - n);

        // if dd is over this constant we have low probability of the success
        if (dd > scc_density) return false;

        std::vector<set_type> scc_list;

        scc_list.reserve(mps_list.n());
        build_kernel_dag(to_add, adj, scc_list);

        nd.suboptimal_extension = set_empty<set_type>();

        int scc_idx = 0;

        for (; scc_idx < scc_list.size() && set_size(scc_list[scc_idx]) <= scc_limit; ++scc_idx) {
            vanilla_bfs(scc_list[scc_idx], nd_id, nd);
        }

        ++scc_idx;

        for (; scc_idx < scc_list.size(); ++scc_idx) {
            nd.suboptimal_extension = nd.suboptimal_extension | scc_list[scc_idx];
        }

        nd.id = nd_id;
        return true;
    } // scc

    inline static int n;

    inline static int scc_limit = 6;
    inline static float scc_density = 0.33;

    inline static MPSList<N> mps_list;
    inline static std::vector<std::pair<uint_type<N>, double>> opt_pa;
    inline static set_type target_id;

}; // struct bnsl_task


template <int N>
inline bool operator==(const bnsl_task<N>& t1, const bnsl_task<N>& t2) {
    return (t1.id == t2.id);
} // operator==

template <int N>
inline std::ostream& operator<<(std::ostream& os, const bnsl_task<N>& t) {
    os.write(reinterpret_cast<const char*>(&t.id), sizeof(t.id));
    os.write(reinterpret_cast<const char*>(&t.score), sizeof(t.score));
    os.write(reinterpret_cast<const char*>(&t.path), sizeof(t.path));
    os.write(reinterpret_cast<const char*>(&t.suboptimal_extension), sizeof(t.suboptimal_extension));
    return os;
} // operator<<

template <int N>
inline std::istream& operator>>(std::istream& is, bnsl_task<N>& t) {
    is.read(reinterpret_cast<char*>(&t.id), sizeof(t.id));
    is.read(reinterpret_cast<char*>(&t.score), sizeof(t.score));
    is.read(reinterpret_cast<char*>(&t.path), sizeof(t.path));
    is.read(reinterpret_cast<char*>(&t.suboptimal_extension), sizeof(t.suboptimal_extension));
    return is;
} // operator>>


namespace std {

  template <int N> struct hash<bnsl_task<N>> {

      std::size_t operator()(const bnsl_task<N>& t) const noexcept {
          return h(t.id);
      } // operator()

      uint_hash h;
  }; // struct hash

} // namespace std


template <int N> struct bnsl_simple_partitioner {
    explicit bnsl_simple_partitioner() { }

    unsigned long long operator()(const bnsl_task<N>& t) const {
        auto val = t.id;
        return h(val);
    } // operator()

    uint_hash h;

}; // struct bnsl_simple_partitioner

template <int N> struct bnsl_hyper_partitioner {
    explicit bnsl_hyper_partitioner() { }

    unsigned long long operator()(const bnsl_task<N>& t) const {
        auto val = t.id;
        val = shift_right(val, (t.n >> 1) + 1);
        return h(val);
    } // operator()

    uint_hash h;

}; // struct bnsl_hyper_partitioner

#endif // BNSL_TASK_HPP
