from abc import abstractmethod
import pickle

from SCoOLBindings import *

class Properties:
    def __getstate__(self):
        attributes = {}
        for k in [a for a in dir(self) if not a.startswith('__')]:
            attributes[k] = self.__getattribute__(k)
        return attributes


class Simple_Partitioner(Simple_Partitioner_Binding):
    def __init__(self) -> None:
        super().__init__()


class Simple_Context(Simple_Context_Binding):
    def __init__(self) -> None:
        super().__init__()


class State(Universal_State):
    propeties: Properties = None

    def __init__(self, properties: Properties = None) -> None:
        super().__init__()
        self.properties = properties

    @classmethod
    def cast(cls, universal_state: Universal_State):
        new_state: cls = cls(pickle.loads(universal_state.get_buffer()))
        return new_state

    @property
    def properties(self) -> Properties:
        buffer = self.get_buffer()
        self._properties = pickle.loads(buffer)
        return self._properties

    @properties.setter
    def properties(self, props: Properties):
        buffer = pickle.dumps(props)
        self.set_buffer(buffer)


class Task(Universal_Task):
    propeties: Properties = None

    def __init__(self, properties: Properties = None) -> None:
        super().__init__()
        self.properties = properties

    @classmethod
    def cast(cls, universal_task: Universal_Task):
        new_task: cls = cls(pickle.loads(universal_task.get_buffer()))
        return new_task

    @property
    def properties(self) -> Properties:
        buffer = self.get_buffer()
        self._properties = pickle.loads(buffer)
        return self._properties

    @properties.setter
    def properties(self, props: Properties):
        buffer = pickle.dumps(props)
        self.set_buffer(buffer)

    def py_process(self, context: object, state: State) -> None:
        state = State.cast(state)
        self.process(context, state)

    def py_merge(self, task: object) -> None:
        task = Task.cast(task)
        self.merge(task)
    
    @abstractmethod
    def process(self, context: object, state: State) -> None:
        raise NotImplementedError("process() is an abstract method")

    @abstractmethod
    def merge(self, task: object) -> None:
        raise NotImplementedError("process() is an abstract method")

class Simple_Executor(Simple_Executor_Binding):
    context: Simple_Context = None
    state: State = None

    def __init__(self) -> None:
        super().__init__()
        self.context = self.get_context()

    def get_executor_context(self) -> None:
        return self.context

    def init(self, task: Task, state: State):
        super().init(task, state)
        self.state = self.get_state()
