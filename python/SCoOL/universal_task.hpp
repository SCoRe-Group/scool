#ifndef UNIVERSAL_TASK_HPP
#define UNIVERSAL_TASK_HPP

#include <pybind11/pybind11.h>

#include "universal_state.hpp"
#include "simple_executor.hpp"

class Py_Universal_Task;
using simple_executor = scool::simple_executor<Py_Universal_Task, Universal_State>;
using simple_context = scool::simple_context<simple_executor, true>;

class Universal_Task {
public:
    Universal_Task() {}

    virtual void py_process(simple_context& ctx, Universal_State& st) = 0;

    virtual void py_merge(const Universal_Task& t) = 0;

    void set_buffer(const pybind11::bytes& buffer) { buffer_ = buffer; }

    pybind11::bytes get_buffer() { return buffer_; }

protected:
    pybind11::bytes buffer_;

}; // class Universal_Task

class Py_Universal_Task : public Universal_Task {
public:
    /* Inherit the constructors */
    using Universal_Task::Universal_Task;

    std::function<void(simple_context &, Universal_State &)> process_func = 0;

    std::function<void(const Universal_Task&)> merge_func = 0;

    Py_Universal_Task() {
        process_func = std::bind(&Py_Universal_Task::py_process,
                                this,
                                std::placeholders::_1,
                                std::placeholders::_2
                            );
        merge_func = std::bind(&Py_Universal_Task::py_merge,
                                this, 
                                std::placeholders::_1
                            );
    }

    void py_process(simple_context& ctx, Universal_State& st) override {
        PYBIND11_OVERRIDE_PURE(
            void,           // Return type
            Universal_Task, // 
            py_process,     // Name of function in C++/Python
            ctx,            // Arguments follow
            st
        );
    }

    void py_merge(const Universal_Task& t) override {
        PYBIND11_OVERRIDE_PURE(
            void,           // Return type
            Universal_Task, // 
            py_merge,       // Name of function in C++/Python
            t               // Arguments follow
        );
    }

    void process(simple_context& ctx, Universal_State& st) {
       if (process_func) process_func(ctx, st); 
    }

    void merge(const Universal_Task& t) {
        if (merge_func) merge_func(t); 
    }

}; // class Py_Universal_Task

#endif // UNIVERSAL_TASK_HPP
