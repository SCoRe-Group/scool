#ifndef UNIVERSAL_STATE_HPP
#define UNIVERSAL_STATE_HPP

#include <pybind11/pybind11.h>

class Universal_State {
public:
    Universal_State() {}

    void identity() {}

    void operator+=(const Universal_State& st);

    void operator==(const Universal_State& st) const;

    void set_buffer(const pybind11::bytes& buffer) { buffer_ = buffer; }

    pybind11::bytes get_buffer() { return buffer_; }

protected:
    pybind11::bytes buffer_;

}; // class Universal_State

std::ostream& operator<<(std::ostream&os, const Universal_State& st);

std::istream& operator>>(std::istream& is, Universal_State& st);

#endif // UNIVERSAL_STATE_HPP
