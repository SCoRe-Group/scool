cmake_minimum_required(VERSION 3.4...3.18)
set(CMAKE_CXX_STANDARD 20)
project(SCoOLBindings)
set(src "SCoOL")
find_package(OpenMP)
find_package(PythonInterp 3)
find_package(PythonLibs 3 REQUIRED)
include_directories(${PYTHON_INCLUDE_DIRS})
include_directories(${CMAKE_SOURCE_DIR}/include)
add_subdirectory(SCoOL/pybind11)

pybind11_add_module(SCoOLBindings SCoOL/bindings.cpp)

if(OpenMP_CXX_FOUND)
    target_link_libraries(SCoOLBindings PUBLIC OpenMP::OpenMP_CXX)
endif()

# include_directories("../include/")

# EXAMPLE_VERSION_INFO is defined by setup.py and passed into the C++ code as a
# define (VERSION_INFO) here.
target_compile_definitions(SCoOLBindings PRIVATE VERSION_INFO=${EXAMPLE_VERSION_INFO})
