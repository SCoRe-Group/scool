from SCoOL import *

class Toy_State(State):
    pass

class Toy_Properties(Properties):
    n = 4
    level = -1
    p = [i for i in range(n)]

class Toy_Task(Task):
    def process(self, context: Simple_Context, state: Toy_State) -> None:
        props: Toy_Properties = self.properties
        props.level += 1

        if (props.level < props.n):
            for i in range(props.level, props.n):
                props.p[i], props.p[props.level] = props.p[props.level], props.p[i]
                new_task = Toy_Task(props)
                context.push(new_task)
                props.p[i], props.p[props.level] = props.p[props.level], props.p[i]

    def merge(self, task: object) -> None:
        pass


def main():
    properties: Properties = Toy_Properties()

    task: Task = Toy_Task(properties)
    state: State = Toy_State()

    executor: Simple_Executor = Simple_Executor()
    executor.init(task, state)

    while(executor.step() > 0):
        pass

if __name__ == "__main__":
    main()
